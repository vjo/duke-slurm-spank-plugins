PACKAGE    ?= duke-slurm-spank-plugins

sysconfdir ?= /etc/slurm/

LIBNAME    ?= lib$(shell uname -m | grep -q x86_64 && echo 64)
BASEDIR    ?= /opt/slurm
LIBDIR     ?= $(BASEDIR)/$(LIBNAME)
INCLUDEDIR ?= $(BASEDIR)/include
BINDIR     ?= /usr/bin
SBINDIR    ?= /sbin
LIBEXECDIR ?= /usr/libexec

export LIBNAME BASEDIR LIBDIR INCLUDEDIR BINDIR SBINDIR LIBEXECDIR PACKAGE

CFLAGS  = -Wall -ggdb

PLUGINS = \
   private-tmpdirs.so

all: $(PLUGINS)

.SUFFIXES: .c .o .so

.c.o: 
	$(CC) $(CFLAGS) -I$(INCLUDEDIR) -L$(LIBDIR) -o $@ -fPIC -c $<
.o.so: 
	$(CC) -shared -o $*.so $<

private-tmpdirs.so: private-tmpdirs.o
	$(CC) -shared -o $*.so private-tmpdirs.o

clean:
	rm -f *.so *.o

install:
	@mkdir -p --mode=0755 $(DESTDIR)$(BASEDIR)/duke-plugins
	@for p in $(PLUGINS); do \
	   echo "Installing $$p in $(BASEDIR)/duke-plugins"; \
	   install -m0755 $$p $(DESTDIR)$(BASEDIR)/duke-plugins; \
	 done
