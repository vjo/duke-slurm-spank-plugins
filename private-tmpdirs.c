/*****************************************************************************
 *  Copyright (C) 2018 Duke University.
 *  Written by Victor J. Orlikowski <vjo@duke.edu>.
 * 
 *  Derived from work produced at Lawrence Livermore National Laboratory
 *  by Jim Garlick <garlick@llnl.gov>.
 *
 *  This is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ****************************************************************************/

/* private-tmpdirs.c - re-direct /tmp, /var/tmp, /scratch, and
 * /dev/shm on a per-task basis */

#define _GNU_SOURCE
#define _XOPEN_SOURCE 500
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <inttypes.h>
#include <pwd.h>
#include <ftw.h>
#include <libgen.h>
#include <sched.h>
#include <sys/mount.h>
#include <slurm/slurm.h>
#include <slurm/spank.h>
#include <sys/stat.h>
#include <errno.h>

#define PATH_SCRATCH_BASE "/scratch/.slurmjobs"
#define PATH_SHM "/dev/shm"

#define JOB_DIR_PREFIX "slurm_"
#define MOUNTPT_MODE        00700

#define PATH_SCRATCH        "/scratch"
#define PATH_TMP            "/tmp"
#define PATH_VARTMP         "/var/tmp"
#define PATH_SCRATCH_SUFFIX ".scratch"
#define PATH_TMP_SUFFIX     ".tmp"
#define PATH_VARTMP_SUFFIX  ".var.tmp"
#define TMP_MODE            01777

#define X11_SUPPORT_FILE_PREFIX "slurm-spank-x11."

#define FDS_USED_FOR_DELETE 64
#define MAX_DELETE_TRIES 10

SPANK_PLUGIN(private-tmpdirs, 1);

#define PROG "private-tmpdirs"

static int _remove_cb(const char *fpath, const struct stat *sb,
                      int typeflag, struct FTW *ftwbuf);
static int _remove_path(char *path);
static int _get_uid(spank_t sp, uid_t *uid);
static int _get_gid(spank_t sp, gid_t *gid);
static int _get_jobid(spank_t sp, uint32_t *jobid);
static int _mkdir_p (char *path, mode_t mode);
static int _construct_jobpath_str(char **path_str, char *base, uint32_t jobid,
                                  char *suffix);
static int _create_directory(char* path, uid_t uid, gid_t gid, mode_t mode);
static int _private_mount(spank_t sp, uid_t uid, gid_t gid, uint32_t jobid);

/* Hooks into SLURM via SPANK interface */

int slurm_spank_task_init_privileged(spank_t sp, int ac, char **av)
{
    int rc = 0;
    uid_t uid;
    gid_t gid;
    uint32_t jobid;

    /* Only run in remote context, please. */
    if (!spank_remote(sp))
        return (0);

    if ((rc = _get_uid(sp, &uid)) < 0) {
        slurm_error("%s: Unable to get user ID: %m", PROG);
        return rc;
    }

    if ((rc = _get_gid(sp, &gid)) < 0) {
        slurm_error("%s: Unable to get group ID: %m", PROG);
        return rc;
    }

    if ((rc = _get_jobid(sp, &jobid)) < 0) {
        slurm_error("%s: Unable to get job ID: %m", PROG);
        return rc;
    }

    return _private_mount(sp, uid, gid, jobid);
}

int slurm_spank_job_epilog(spank_t sp, int ac, char **av)
{
    int rc = 0;
    int try_count = 0;
    int jobbase_path_exists = 0;
    uint32_t jobid;
    char *jobbase_path = NULL;

    /* Only run in job_script context, please. */
    if (S_CTX_JOB_SCRIPT != spank_context())
        return (0);

    /* FIXME: Write a cleanup script to run at slurmd start */
    if ((rc = _get_jobid(sp, &jobid)) < 0) {
        slurm_error("%s: Unable to get job ID: %m", PROG);
        slurm_error("%s: No further cleanup will be attempted.", PROG);
        goto exit_cleanup;
    }

    slurm_info("%s: Attempting automatic cleanup of re-mapped directories.", PROG);
    if ((rc = _construct_jobpath_str(&jobbase_path, PATH_SCRATCH_BASE,
                                     jobid, NULL)) < 0) {
        slurm_error("%s: Unable generate path string based on %s: %m",
                    PROG, PATH_SCRATCH_BASE);
        goto exit_cleanup;
    }
    else {
        while (1) {
            rc = _remove_path(jobbase_path);

            try_count++;
            jobbase_path_exists = access(jobbase_path, F_OK);

            if ((0 != jobbase_path_exists) ||
                (try_count >= MAX_DELETE_TRIES)) {
                break;
            }
            else {
                slurm_info("%s: Attempt %d to delete %s failed.",
                           PROG, try_count, jobbase_path);
                slurm_info("%s: Retrying in 1 second.", PROG);
                sleep(1);
            }
        }

        if (0 == jobbase_path_exists) {
            slurm_error("%s: Deletion of %s failed after %d attempts.",
                        PROG, jobbase_path, MAX_DELETE_TRIES);
        }
        else {
            slurm_info("%s: Attempt at deletion of %s succeeded.",
                       PROG, jobbase_path);
        }
    }

  exit_cleanup:
    free(jobbase_path);
    slurm_info("%s: Done attempting to clean up re-mapped directories.", PROG);
    /* Return code doesn't matter, in this case; log messages much
     * more important. */
    return 0;
}

/* Support functions */

static int _remove_cb(const char *fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf)
{
    int rc = 0;

    /* Capture the return code, but don't actually use it.
       Just log the error, then return success later, so that we can proceed
       with finishing the attempt at deleting the entire directory tree. */
    if ((rc = remove(fpath)) < 0)
        slurm_error("%s: failed to remove %s due to: %m", PROG, fpath);

    return 0;
}

static int _remove_path(char *path)
{
    return nftw(path, _remove_cb, FDS_USED_FOR_DELETE, FTW_DEPTH | FTW_PHYS);
}

static int _get_uid(spank_t sp, uid_t *uid)
{
    if (spank_remote(sp)) {
        if (spank_get_item(sp, S_JOB_UID, uid) != ESPANK_SUCCESS) {
            slurm_error("%s: could not obtain job uid", PROG);
            return (-1);
        }
    } else {
        *uid = getuid();
    }

    return 0;
}

static int _get_gid(spank_t sp, gid_t *gid)
{
    if (spank_remote(sp)) {
        if (spank_get_item(sp, S_JOB_GID, gid) != ESPANK_SUCCESS) {
            slurm_error("%s: could not obtain job gid", PROG);
            return (-1);
        }
    } else {
        *gid = getgid();
    }

    return 0;
}

static int _get_jobid(spank_t sp, uint32_t *jobid)
{
    if (spank_get_item(sp, S_JOB_ID, jobid) != ESPANK_SUCCESS) {
        slurm_error("%s: could not obtain job id", PROG);
        return (-1);
    }

    return 0;
}

static int _mkdir_p (char *path, mode_t mode)
{
    int rc;
    char *cpy;
    mode_t saved_umask;
    struct stat sb;

    saved_umask = umask(0022);
    if ((rc = mkdir(path, mode)) < 0) {
        switch (errno) {
            case ENOENT:
                if (!(cpy = strdup(path))) {
                    errno = ENOMEM;
                    rc = -1;
                    break;
                }
                if ((rc = _mkdir_p(dirname(cpy), mode)) == 0)
                    rc = mkdir(path, mode);
                free(cpy);
                break;
            case EEXIST:
                if (stat(path, &sb) == 0 && S_ISDIR(sb.st_mode))
                    rc = 0;
                break;
        }
    }
    umask(saved_umask);

    return rc;
}

static int _construct_jobpath_str(char **path_str, char *base,
                                  uint32_t jobid, char *suffix)
{
    int rc = 0;
    size_t base_path_len = 0;
    size_t suffixed_path_len = 0;
    char *base_path_str = NULL;
    char *suffixed_path_str = NULL;
    char *base_path_format_str = "%s/%s%"PRIu32;

    base_path_len = snprintf(NULL, 0, base_path_format_str,
                             base, JOB_DIR_PREFIX, jobid);

    if (base_path_len < 0) {
        rc = -1;
        goto jp_cleanup;
    }

    base_path_len++; /* Account for null terminator. */
    base_path_str = malloc(base_path_len);
    if (!base_path_str) {
        rc = -1;
        goto jp_cleanup;
    }

    rc = snprintf(base_path_str, base_path_len, base_path_format_str,
                  base, JOB_DIR_PREFIX, jobid);

    if ((rc < 0) || (rc >= base_path_len )) {
        rc = -1;
        goto jp_cleanup;
    }

    if (suffix) {
        suffixed_path_len = snprintf(NULL, 0, "%s/%s",
                                     base_path_str, suffix);
        if (suffixed_path_len < 0) {
            rc = -1;
            goto jp_cleanup;
        }
        suffixed_path_len++; /* Account for null terminator. */
        suffixed_path_str = malloc(suffixed_path_len);
        if (!suffixed_path_str) {
            rc = -1;
            goto jp_cleanup;
        }
        rc = snprintf(suffixed_path_str, suffixed_path_len, "%s/%s",
                      base_path_str, suffix);
        if ((rc < 0) || (rc >= suffixed_path_len)) {
            rc = -1;
            goto jp_cleanup;
        }
    }

    *path_str = (suffixed_path_str ? suffixed_path_str : base_path_str);

  jp_cleanup:
    if (suffixed_path_str != *path_str) free(suffixed_path_str);
    if (base_path_str != *path_str) free(base_path_str);
    return rc;
}

static int _create_directory(char* path, uid_t uid, gid_t gid, mode_t mode)
{
    int rc = 0;
    
    if ((rc = _mkdir_p(path, mode)) < 0) {
        slurm_error("%s: Unable to mkdir %s: %m", PROG, path);
        return rc;
    }
    if ((rc = chown(path, uid, gid)) < 0) {
        slurm_error("%s: Unable to chown %s: %m", PROG, path);
        return rc;
    }

    return rc;
}

static int _private_mount(spank_t sp, uid_t uid, gid_t gid, uint32_t jobid)
{
    int rc = 0;
    char *jobbase_path = NULL;
    char *tmp_path = NULL;
    char *vartmp_path = NULL;
    char *scratch_path = NULL;
    char *shm_path = NULL;
    struct stat s;

    /* Perform sanity checks... */
    if (stat(PATH_SCRATCH_BASE, &s) < 0) {
        slurm_error("%s: stat of %s failed: %m", PROG, PATH_SCRATCH_BASE);
        return (-1);
    }
    
    if (!S_ISDIR(s.st_mode)) {
        slurm_error("%s: %s is not a directory.",
                    PROG, PATH_SCRATCH_BASE);
        return (-1);
    }

    if (s.st_uid != 0) {
        slurm_error("%s: %s has wrong ownership; must be owned by root.",
                    PROG, PATH_SCRATCH_BASE);
        return (-1);
    }
    
    if (s.st_mode & S_IWOTH) {
        slurm_error("%s: %s has wrong permissions; o-w is required.",
                    PROG, PATH_SCRATCH_BASE);
        return (-1);
    }

    if (access(PATH_SCRATCH_BASE, (W_OK | X_OK)) < 0) {
        slurm_error("%s: %s not writable or cannot be entered; check permissions or ACLs.",
                    PROG, PATH_SCRATCH_BASE);
        return (-1);
    }
    
    if (stat(PATH_SHM, &s) < 0) {
        slurm_error("%s: stat of %s failed: %m", PROG, PATH_SHM);
        return (-1);
    }

    if (!S_ISDIR(s.st_mode)) {
        slurm_error("%s: %s is not a directory.", PROG, PATH_SHM);
        return (-1);
    }

    /* Construct path names */
    if ((rc = _construct_jobpath_str(&jobbase_path, PATH_SCRATCH_BASE,
                                     jobid, NULL)) < 0) {
        slurm_error("%s: Unable to generate path string based on %s",
                    PROG, PATH_SCRATCH_BASE);
        goto mount_cleanup;
    }

    if ((rc = _construct_jobpath_str(&tmp_path, PATH_SCRATCH_BASE,
                                     jobid, PATH_TMP_SUFFIX)) < 0) {
        slurm_error("%s: Unable to generate path string based on %s",
                    PROG, PATH_SCRATCH_BASE);
        goto mount_cleanup;
    }

    if ((rc = _construct_jobpath_str(&vartmp_path, PATH_SCRATCH_BASE,
                                     jobid, PATH_VARTMP_SUFFIX)) < 0) {
        slurm_error("%s: Unable to generate path string based on %s",
                    PROG, PATH_SCRATCH_BASE);
        goto mount_cleanup;
    }

    if ((rc = _construct_jobpath_str(&scratch_path, PATH_SCRATCH_BASE,
                                     jobid, PATH_SCRATCH_SUFFIX)) < 0) {
        slurm_error("%s: Unable to generate path string based on %s",
                    PROG, PATH_SCRATCH_BASE);
        goto mount_cleanup;
    }

    if ((rc = _construct_jobpath_str(&shm_path, "", jobid, NULL)) < 0) {
        slurm_error("%s: Unable to generate path string for SHM mount.", PROG);
        goto mount_cleanup;
    }

    /* Create directories */
    if ((rc = _create_directory(jobbase_path, uid, gid, MOUNTPT_MODE)) < 0) {
        goto mount_cleanup;
    }
    slurm_info("%s: Parent directory %s for re-mapped directories created.",
               PROG, jobbase_path);

    if ((rc = _create_directory(tmp_path, uid, gid, TMP_MODE)) < 0) {
        goto mount_cleanup;
    }
    slurm_info("%s: /tmp re-map directory %s created.",
               PROG, tmp_path);

    if ((rc = _create_directory(vartmp_path, uid, gid, TMP_MODE)) < 0) {
        goto mount_cleanup;
    }
    slurm_info("%s: /var/tmp re-map directory %s created.",
               PROG, vartmp_path);

    if ((rc = _create_directory(scratch_path, uid, gid, MOUNTPT_MODE)) < 0) {
        goto mount_cleanup;
    }
    slurm_info("%s: /scratch re-map directory %s created.",
               PROG, scratch_path);

    /* Create new namespaces */
    if (unshare(CLONE_NEWNS) < 0) {
        slurm_error("%s: unshare CLONE_NEWNS: %m", PROG);
        rc = -1;
        goto mount_cleanup;
    }

    if (unshare(CLONE_NEWIPC) < 0) {
        slurm_error("%s: unshare CLONE_NEWIPC: %m", PROG);
        rc = -1;
        goto mount_cleanup;
    }

    /* Protect the parent namespace. */
    if (mount("none", "/", NULL, MS_SLAVE | MS_REC, NULL)) {
        slurm_error("%s: mount MS_SLAVE: %m", PROG);
        rc = -1;
        goto mount_cleanup;
    }
    slurm_info("%s: Parent namespace protected.", PROG);

    /* Mount directories and set environment variables */
    if ((rc = mount(tmp_path, PATH_TMP, NULL, MS_BIND, NULL)) < 0) {
        slurm_error("%s: private mount --bind %s %s: %m",
                    PROG, tmp_path, PATH_TMP);
        goto mount_cleanup;
    }

    if (spank_setenv(sp, "TMPDIR", PATH_TMP, 1) != ESPANK_SUCCESS) {
        slurm_error("%s: setenv(TMPDIR, \"%s\"): %m", PROG, PATH_TMP);
        rc = -1;
        goto mount_cleanup;
    }

    slurm_info("%s: %s mapped to: %s", PROG, PATH_TMP, tmp_path);

    if ((rc = mount(vartmp_path, PATH_VARTMP, NULL, MS_BIND, NULL)) < 0) {
        slurm_error("%s: private mount --bind %s %s: %m",
                    PROG, vartmp_path, PATH_VARTMP);
        goto mount_cleanup;
    }

    slurm_info("%s: %s mapped to: %s", PROG, PATH_VARTMP, vartmp_path);

    if ((rc = mount(scratch_path, PATH_SCRATCH, NULL, MS_BIND, NULL)) < 0) {
        slurm_error("%s: private mount --bind %s %s: %m",
                    PROG, scratch_path, PATH_SCRATCH);
        goto mount_cleanup;
    }

    slurm_info("%s: %s mapped to: %s", PROG, PATH_SCRATCH, scratch_path);

    /* Create new shm mount in this namespace. */
    if ((rc = mount(shm_path, PATH_SHM, "tmpfs",
                    (MS_NOSUID | MS_NOEXEC | MS_RELATIME), "mode=01777"))) {
        slurm_error("%s: private mount of %s failed: %m", PROG, PATH_SHM);
        goto mount_cleanup;
    }
    slurm_info("%s: Private %s mounted.", PROG, PATH_SHM);

  mount_cleanup:
    free(jobbase_path);
    free(tmp_path);
    free(vartmp_path);
    free(scratch_path);
    free(shm_path);
    return rc;
}
