%define _build_id_links none

Name: duke-slurm-spank-plugins   
Version: 0.2
Release: 14

Summary: SLURM SPANK plugins for Duke Research Computing
Group: System Environment/Base
License: GPL

BuildRoot: %{_tmppath}/%{name}-%{version}
Source: %{name}-%{version}.tgz

Requires: slurm
BuildRequires: slurm-devel

%description
This package contains a set of SLURM spank plugins which enhance and
extend SLURM functionality for users and administrators at Duke University.

Currently includes:
 - private-tmpdirs.so : Re-map /tmp, /var/tmp, /dev/shm, and /scratch;
                        set TMPDIR to re-mapped /tmp. After job completion,
                        clean up directories into which the above were mapped.

%prep
%setup

%build
make CFLAGS="$RPM_OPT_FLAGS" 

%install
rm -rf "$RPM_BUILD_ROOT"
mkdir -p "$RPM_BUILD_ROOT"

make \
  BINDIR=%{_bindir} \
  SBINDIR=/sbin \
  LIBEXECDIR=%{_libexecdir} \
  DESTDIR="$RPM_BUILD_ROOT" \
  install

%clean
rm -rf "$RPM_BUILD_ROOT"

%files 
%defattr(-,root,root,0755)
%doc README
/opt/slurm/duke-plugins/private-tmpdirs.so

%changelog
* Thu Jul 27 2023 Victor J. Orlikowski <vjo@duke.edu> - 0.2-14
- Rebuild for SLURM 23.02.3

* Mon Nov 28 2022 Zach Hill <zach.hill@duke.edu> - 0.2-13
- Rebuild for SLURM 22.05.6

* Tue Jul 27 2021 Victor J. Orlikowski <vjo@duke.edu> - 0.2-12
- Handle directory cleanup in epilog context.
- Try deletion multiple times, in the event of transient failure.
- Remove need for /.hiddenscratch directory.

* Thu May 13 2021 Victor J. Orlikowski <vjo@duke.edu> - 0.2-11
- Fix mis-use of nftw() that resulted in directories escaping cleanup.

* Thu May 13 2021 Victor J. Orlikowski <vjo@duke.edu> - 0.2-10
- Bump version number for re-compile/re-deploy on 20.11.6

* Sun Dec 20 2020 Victor J. Orlikowski <vjo@duke.edu> - 0.2-9
- Remove build-id files on CentOS 8

* Fri Mar 04 2020 Victor J. Orlikowski <vjo@duke.edu> - 0.2-8
- Re-map for every job step, but cleanup only at exit.

* Fri Mar 04 2020 Victor J. Orlikowski <vjo@duke.edu> - 0.2-7
- Sigh. Fixing broken cleanup logic, that broke re-mapped directories after job steps.

* Mon Feb 24 2020 Victor J. Orlikowski <vjo@duke.edu> - 0.2-6
- Removing plugstack.conf.d directory; causes more problems than it solves.

* Wed Feb 19 2020 Victor J. Orlikowski <vjo@duke.edu> - 0.2-5
- Trying with only per-job private tmp

* Thu Dec 19 2019 Victor J. Orlikowski <vjo@duke.edu> - 0.2-4
- Bump version to deal with minor DCC snafu

* Tue Dec 3 2019 Victor J. Orlikowski <vjo@duke.edu> - 0.2-3
- Removed copy of X11 support files, since no longer needed with 19.05

* Tue Jun 4 2019 Victor J. Orlikowski <vjo@duke.edu> - 0.2-2
- Bump - need to handle undefined behavior for fclose(NULL).

* Mon Jun 3 2019 Victor J. Orlikowski <vjo@duke.edu> - 0.2-1
- Version bump for feature addition; need to work around X11 forwarding plugin issue.

* Mon Dec 10 2018 Victor J. Orlikowski <vjo@duke.edu> - 0.1-3
- Bump - need to deal with different job steps.

* Fri Dec 7 2018 Victor J. Orlikowski <vjo@duke.edu> - 0.1-2
- Minor rev bump, to deal with bug in job arrays.

* Thu Dec 6 2018 Victor J. Orlikowski <vjo@duke.edu> - 0.1-1
- Initial release 0.1
